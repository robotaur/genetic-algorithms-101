﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Genetic_Algorithms_101
{
    class Program
    {
        static void Main(string[] args)
        {
            // Initialise algorithm
            Algorithm.Init();

            // Set candidate solution
            string candidateSolution = "1111000000000001101000000000000000011110110000100011000000001111";
            FitnessCalculator.SetSolution(candidateSolution);

            Console.WriteLine("Candidate solution: ");
            Console.WriteLine(candidateSolution);
            Console.WriteLine();

            // Initialise original population
            Population population = new Population(50, true);

            Console.WriteLine("Starting...");

            // Evolve population until we reach optimum solution
            int generation = 0;
            while (population.GetFittest().GetFitness() < FitnessCalculator.GetOptimumFitness())
            {
                generation++;
                Console.WriteLine("Generation: " + generation + ", Fitness: " + population.GetFittest().GetFitnessPercentage() + "%");
                Console.WriteLine(population.GetFittest().ToString());
                population = Algorithm.EvolvePopulation(population);
            }

            Console.WriteLine();

            Console.WriteLine("Solution found in generation: " + generation);
            Console.WriteLine("Gene sequence: ");
            Console.WriteLine(population.GetFittest().ToString());

            Console.ReadLine();
        }
    }
}
