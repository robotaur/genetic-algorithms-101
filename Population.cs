﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Genetic_Algorithms_101
{
    public class Population
    {
        private Individual[] individuals;
        
        // CTOR
        public Population(int populationSize, bool init)
        {
            individuals = new Individual[populationSize];

            if (init)
            {
                Random rand = new Random();
                for (int i = 0; i < populationSize; i++)
                {
                    Individual individual = new Individual();
                    individual.GenerateIndividual(rand);
                    SaveIndividual(i, individual);
                }
            }
        }

        #region Public Methods
        public void SaveIndividual(int index, Individual individual)
        {
            individuals[index] = individual;
        }

        public Individual GetIndividual(int index)
        {
            return individuals[index];
        }

        public Individual GetFittest()
        {
            Individual fittest = GetIndividual(0);
            int fitnessRating = fittest.GetFitness();   // The fitness rating for the fittest individual is stored seperately to avoid
                                                        // calculating it for each individual check in the loop

            foreach (Individual check in individuals)
            {
                int checkFitness = check.GetFitness();
                if (checkFitness > fitnessRating)
                {
                    fitnessRating = checkFitness;
                    fittest = check;
                }
            }

            return fittest;
        }

        public int Size()
        {
            return individuals.Length;
        }
        #endregion
    }
}
