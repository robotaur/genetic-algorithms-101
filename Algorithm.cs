﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Genetic_Algorithms_101
{
    public class Algorithm
    {
        // Base GA Parameters
        private const double uniformRate = 0.5;
        private const double mutationRate = 0.015;
        private const int tournamentSize = 5;
        private const bool elitist = true;

        // Fields
        private static Random rand;

        // Init
        public static void Init()
        {
            rand = new Random();
        }

        // Evolve a population
        public static Population EvolvePopulation(Population pop)
        {
            Population newPopulation = new Population(pop.Size(), false);

            // Keep best individual?
            if (elitist) newPopulation.SaveIndividual(0, pop.GetFittest());

            // Crossover population
            int elitistOffset = elitist ? 1 : 0;

            // Loop population and create crossover individuals
            for (int i = elitistOffset; i < pop.Size(); i++)
            {
                Individual individual1 = tournamentSelection(pop);
                Individual individual2 = tournamentSelection(pop);
                Individual childIndividual = crossover(individual1, individual2);

                newPopulation.SaveIndividual(i, childIndividual);
            }

            // Mutate population
            for (int i = elitistOffset; i < newPopulation.Size(); i++)
            {
                mutate(newPopulation.GetIndividual(i));
            }

            return newPopulation;
        }

        // Crossover individuals
        private static Individual crossover(Individual individual1, Individual individual2)
        {
            Individual childIndividual = new Individual();

            //Loop all genes and crossover
            for (int i = 0; i < individual1.Size(); i++)
            {
                if (rand.NextDouble() <= uniformRate)
                    childIndividual.SetGene(i, individual1.GetGene(i));
                else
                    childIndividual.SetGene(i, individual2.GetGene(i));
            }

            return childIndividual;
        }

        // Mutate individual
        private static void mutate(Individual individual)
        {
            // Loop all genes and generate new gene where mutation occurs
            for (int i = 0; i < individual.Size(); i++)
            {
                if (rand.NextDouble() <= mutationRate)
                {
                    byte newGene = (byte)Math.Round(rand.NextDouble());
                    individual.SetGene(i, newGene);
                }
            }
        }

        // Select individuals for crossover
        private static Individual tournamentSelection(Population pop)
        {
            // Create tournament population
            Population tournament = new Population(tournamentSize, false);

            // Put random individuals from the original population into the tournament population
            for (int i = 0; i < tournamentSize; i++)
            {
                int randomId = rand.Next(0, pop.Size());
                tournament.SaveIndividual(i, pop.GetIndividual(randomId));
            }

            // Get fittest individual from tournament population
            Individual fittest = tournament.GetFittest();
            return fittest;
        }
    }
}