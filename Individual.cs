﻿using System;
using System.Text;

namespace Genetic_Algorithms_101
{
    public class Individual
    {
        private static int defaultGeneLength = 64;
        private byte[] genes = new byte[defaultGeneLength];

        // Cache
        private int fitness = 0;

        // Create a random individual
        public void GenerateIndividual(Random rand)
        {
            for (int i = 0; i < genes.Length; i++)
            {
                byte gene = (byte)Math.Round(rand.NextDouble());
                genes[i] = gene;
            }
        }

        #region Public Methods
        public static void SetDefaultGeneLength(int geneLength)
        {
            Individual.defaultGeneLength = geneLength;
        }

        public byte GetGene(int index)
        {
            return genes[index];
        }

        public void SetGene(int index, byte value)
        {
            genes[index] = value;
            fitness = 0;
        }

        public int GetFitness()
        {
            if (fitness == 0)
                fitness = FitnessCalculator.GetFitness(this);

            return fitness;
        }

        public int GetFitnessPercentage()
        {
            return (int)Math.Round((Convert.ToDouble(this.GetFitness()) / Convert.ToDouble(FitnessCalculator.GetOptimumFitness())) * 100.0);
        }

        public int Size()
        {
            return genes.Length;
        }

        // Build string from gene sequence
        public override string ToString()
        {
            StringBuilder geneString = new StringBuilder();
            for (int i = 0; i < genes.Length; i++)
            {
                geneString.Append(this.GetGene(i));
            }

            return geneString.ToString();
        }
        #endregion
    }
}
