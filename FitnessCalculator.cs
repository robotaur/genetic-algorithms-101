﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Genetic_Algorithms_101
{
    public class FitnessCalculator
    {
        private static byte[] solution;

        // Set candidate solution
        public static void SetSolution(byte[] newSolution)
        {
            solution = newSolution;
        }

        // Helper method to set candidate solution from string
        public static void SetSolution(string newSolution)
        {
            solution = new byte[newSolution.Length];

            for (int i = 0; i < newSolution.Length; i++)
            {
                String character = newSolution.Substring(i, 1);
                if (character == "0" || character == "1")
                    solution[i] = Byte.Parse(character);
                else
                    solution[i] = 0;
            }
        }

        // Get fitness of individual by looping all genes and comparing
        // each to the candidate solution
        public static int GetFitness(Individual individual)
        {
            int fitness = 0;

            for (int i = 0; i < individual.Size(); i++)
            {
                if (individual.GetGene(i) == solution[i])
                    fitness++;
            }

            return fitness;
        }

        // Get optimum fitness
        public static int GetOptimumFitness()
        {
            return solution.Length;
        }
    }
}
